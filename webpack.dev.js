const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

/** @type {import('webpack').Configuration} */

module.exports = {
  mode: "development",
  entry: {
    webpack: "./src/webpackMain.js",
  },
  output: {
    filename: "[name].[contenthash].js",
    assetModuleFilename: "assets/images/[name][hash][ext][query]",
  },
  module: {
    rules: [
      {
        test: /\.(svg|png)$/i,
        type: "asset/inline",
        //type: "asset/resource",
      },
      {
        test: /\.(jpe?g|webp|)$/i,
        exclude: /node_modules/,
        use: [
          {
            loader: "img-optimize-loader",
            options: {
              compress: {
                mode: "low",
                webp: {
                  quality: 30,
                },
              },
              name: "[name][hash].[ext]",
              outputPath: "assets/images/",
            },
          },
        ],
        //type: "asset/source",
      },
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.css|.s[ac]ss$/i,
        exclude: /node_modules/,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Emprendiendo Soluciones",
      template: "./src/index.html",
      favicon: "./src/img/favicon/favicon.ico",
    }),
    new MiniCssExtractPlugin({
      filename: "assets/styles/[name].[contenthash].css",
    }),
  ],
  devServer: {
    host: "0.0.0.0",
    open: false,
  },
};
