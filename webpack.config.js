/* 
// Webpack config common
*/
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

/** @type {import('webpack').Configuration} */

module.exports = {
  entry: {
    webpack: "./src/webpackMain.js",
  },
  module: {
    rules: [
      {
        test: /\.(jpe?g|webp|)$/i,
        exclude: /node_modules/,
        use: [
          {
            loader: "img-optimize-loader",
            options: {
              compress: {
                webp: true,
                disableOnDevelopment: true,
                svgo: false,
              },
            },
          },
        ],
      },
      {
        test: /\.(svg|png)$/i,
        type: "asset/inline",
      },
      {
        test: /\.(sa|sc|c)ss$/,
        exclude: /node_modules/,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Emprendiendo Soluciones",
      template: "./src/index.html",
      favicon: "./src/img/favicon/favicon.ico",
    }),
    new MiniCssExtractPlugin(),
  ],
};
