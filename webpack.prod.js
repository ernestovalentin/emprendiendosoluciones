const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

/** @type {import('webpack').Configuration} */

module.exports = {
  mode: "production",
  entry: {
    webpack: "./src/webpackMain.js",
  },
  output: {
    filename: "assets/scripts/main.js",
    publicPath: "https://emprendiendosoluciones.com/",
    //publicPath:
    //  "file:///home/elementary/Development/emprendiendosoluciones/website/dist/",
    assetModuleFilename: "assets/images/[name][ext][query]",
    clean: true,
  },
  module: {
    rules: [
      {
        test: /\.(svg|png)$/i,
        type: "asset/inline",
      },
      {
        test: /\.(jpe?g|webp|)$/i,
        exclude: /node_modules/,
        use: [
          {
            loader: "img-optimize-loader",
            options: {
              compress: {
                mode: "high",
                webp: {
                  quality: 90,
                  metadata: "none",
                },
              },
              name: "[name].[ext]",
              outputPath: "assets/images/",
            },
          },
        ],
      },
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.css|.s[ac]ss$/i,
        exclude: /node_modules/,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Emprendiendo Soluciones",
      template: "./src/index.html",
      favicon: "./src/img/favicon/favicon.ico",
    }),
    new MiniCssExtractPlugin({
      filename: "assets/styles/style.css",
    }),
  ],
  optimization: {
    minimize: true,
  },
};
