let menu = document.querySelector(".menu");
let hamb = document.querySelector(".hamb");
let background = document.querySelector(".backgroundmenu");
let logo = document.querySelector(".logo");
let header = document.querySelector("header");
let desliza = document.querySelector("#downmodal");
let Allteam = document.querySelectorAll(".carousel-cell");

new fullpage("#fullpage", {
  anchors: ["inicio", "who", "todo", "projects", "team", "contact", "qr"],
  menu: ".menu",
  navigation: true,
  onLeave: function (origin, destination) {
    menu.classList.remove("active");
    hamb.classList.remove("active");
    background.classList.remove("active");
    if (origin.anchor == "inicio") {
      logo.style.opacity = 1;
      header.style.background = "rgba(0, 0, 0, 0.2)";
      desliza.style.opacity = 0;
    } else if (destination.anchor == "inicio") {
      logo.style.opacity = 0;
      header.style.background = "rgba(0, 0, 0, 0)";
      desliza.style.opacity = 1;
    }
  },
});

background.onclick = (e) => {
  hamb.classList.remove("active");
  menu.classList.remove("active");
  background.classList.remove("active");
};

hamb.onclick = (e) => {
  hamb.classList.toggle("active");
  menu.classList.toggle("active");
  background.classList.toggle("active");
};

function ramdomTeam(teams) {
  let orderNumber = [];
  teams.forEach((team) => {
    let ramdom = Math.random().toFixed(2).substring(2);
    team.setAttribute("orderTeam", ramdom);
    orderNumber.push(ramdom);
  });
  orderNumber.sort(function (a, b) {
    return a - b;
  });
  //Debe poder mover los divs
  // documentDivBefore.after(documentDivAfter);
}
